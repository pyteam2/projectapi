from flask import Flask
from flask_mongoengine import MongoEngine, MongoEngineSessionInterface

app = Flask(__name__)

app.config['MONGODB_DB'] = 'projectbase'
app.config['MONGODB_HOST'] = 'ds119738.mlab.com'
app.config['MONGODB_PORT'] = 19738
app.config['MONGODB_USERNAME'] = 'project'
app.config['MONGODB_PASSWORD'] = '12345'


#register_connection('default', 'mongoenginetest')

db = MongoEngine(app)
app.session_interface = MongoEngineSessionInterface(db)

import projectapi.project