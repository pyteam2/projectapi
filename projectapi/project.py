from projectapi import app
from flask import render_template, request, jsonify
from projectapi.models import Series2, KeyAccounts
from os import urandom
from client_api_key import api_key

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/show_all', methods=['GET'])
def show_all():
    if validate_key():
        output = []
        for data in Series2.objects():
            output.append(data)
        return jsonify(output)
    else:
        return jsonify('No api key')
		
@app.route('/search_by_actors', methods = ['GET'])
def search_actor():
    if validate_key():
        actors_name = request.args.get('ac')
        if (actors_name.replace(' ', '')):
            actors_name = actors_name.replace('+', ' ')
            actors = []
            for data in Series2.objects(actors__name__icontains=actors_name).only('title', 'actors.name'):
                actors.append({'title': data['title']})
            return jsonify(actors)
        else:
            return jsonify('Error: Please specify a name')
    else:
        return jsonify('No api key')

@app.route('/get_key', methods=['GET'])
def get_key():
    key=urandom(16).encode('hex')
    key_exists = KeyAccounts.objects(api_key=key)
    if (not key_exists):
        register = KeyAccounts(api_key=key, status='Active')
        register.save()
        return "Success! Key =  {}".format(key)
    else:
        get_key()

@app.route('/check_key', methods=['GET'])
def is_key_valid():
    if validate_key():
        req_key = request.args.get('key')
        req_key=req_key.replace(' ','')
        req_key=req_key.replace('+','')
        keyobj=check_key(req_key)
        if (keyobj):
            key_found=keyobj[0]
            return jsonify({'key' : key_found['api_key'], 'status' : key_found['status']})
        else:
            return "Key {} not found".format(req_key)
    else:
        return jsonify('No api key')

def validate_key():
    if check_key(api_key):
        return True
    else:
        return False

def check_key(key):
    key_exists = KeyAccounts.objects(__raw__={'api_key': key}).only('api_key', 'status')
    return key_exists

@app.route('/show_keys', methods=['GET'])
def show_keys():
    if validate_key():
        key_exists = KeyAccounts.objects().only('api_key', 'status')
        keys=[]
        for data in key_exists:
            keys.append({'key': data['api_key'], 'status': data['status']})
        return jsonify(keys)
    else:
        return jsonify('No api key')



@app.route('/search_by_seasons', methods = ['GET'])
def search_seasons():
    if validate_key():
        try:
            season_number=int(request.args.get('s'))
            series_found = []
            for data in Series2.objects(season__sid__icontains=season_number).only('title', 'rating', 'genre', 'season.sid'):
                b=[]
                for a in data['season']:
                    b.append(a['sid'])
                l=['sid']
                c=dict(zip(l,b))
                series_found.append({'title': data['title'], 'rating': data['rating'], 'genre': data['genre'], 'seasons': c})
            return jsonify(series_found)
        except:
            return jsonify('Error: Wrong Season number')
    else:
        return jsonify('No api key')

@app.route('/search_ep', methods = ['GET']) #erevna ean yparxei epeisodio no tade se kathe seira
                          #kai epistrefei th seira pou exei ton sigkekrimeno arithmo
def search_ep():
    if validate_key():
        try:
            no_episode = []
            episode_number = int(request.args.get('e'))
            for data in Series2.objects(__raw__={'season.episodes.number': episode_number}):
                no_episode.append({'title': data['title']})
            return jsonify({'data': no_episode})
        except:
            return jsonify('Error: Wrong Episode number')
    else:
        return jsonify('No api key')

@app.route('/search_by_genre', methods = ['GET']) #anazhthsh me vash to eidos ths seiras,epistrefei tis seires me to eidos
                                                  # pou dhlwthike(horror,action,drama,adventure,thriller)
def search_gen():
    if validate_key():
        genre_series = request.args.get('g')
        genre = []
        for data in Series2.objects(genre__type__icontains=genre_series).only('title', 'rating'):
            genre.append({'title': data['title'], 'rating': data['rating']})
        return jsonify({'data': genre})
    else:
        return jsonify('No api key')

@app.route('/search_by_title', methods = ['GET'])
def search_by_title():
    serie_title = request.args.get('q')
    series_found = []
    for data in Series2.objects(title__icontains=serie_title).only('title', 'rating', 'genre', 'season.sid'):
        b=[]
        l = ['sid']
        c = dict(zip(l, b))
        series_found.append({'title': data['title'], 'rating': data['rating'], 'genre': data['genre'], 'seasons': c})
    return jsonify(series_found)

@app.route('/top_rated')
def top_rated():
    limit = 3
    data = Series2.objects().only('title', 'rating').order_by('-rating')
    data = data[:limit].to_json()
    return jsonify(data)


@app.route('/next_episode', methods=['GET'])
def next_episode():
    next_episode_number = None
    current_title = str(request.args.get('ct'))
    current_season = int(request.args.get('cs'))
    current_episode = int(request.args.get('ce'))
    data = Series2.objects(title=current_title).only('season.episodes.number')
    for record in data[0]['season'][0]['episodes']:
        if int(record['number']) == current_episode + 1:
            next_episode_number = int(record['number'])

    return jsonify(
        {'next_episode': next_episode_number, 'current_title': current_title, 'current_season': current_season})