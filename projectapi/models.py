from projectapi import db

class Episodes(db.EmbeddedDocument):
    number = db.StringField()
    published_date = db.StringField()

class Seasons(db.EmbeddedDocument):
    sid = db.IntField()
    episodes = db.EmbeddedDocumentListField(Episodes)

class  Genres(db.EmbeddedDocument):
    type = db.StringField()

class Actors(db.EmbeddedDocument):
    name=db.StringField()

class Series2(db.Document):
    meta = {'collection': 'Series2'}
    title = db.StringField()
    rating = db.FloatField()
    actors = db.EmbeddedDocumentListField(Actors)
    genre = db.EmbeddedDocumentListField(Genres)
    season = db.EmbeddedDocumentListField(Seasons)

class KeyAccounts(db.Document):
    meta = {'collection': 'KeyAccounts'}
    api_key = db.StringField(unique=True)
    status = db.StringField()