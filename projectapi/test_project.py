import unittest
from projectapi import app
import json

class ProjectTestCase(unittest.TestCase):

    def test_wrong_search_seasons(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_by_seasons?s=sgfdsg', content_type='application/json')
        out= json.loads(response.data)
        self.assertEqual(out, 'Error: Wrong Season number')

    def test_results_search_seasons(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_by_seasons?s=7', content_type='application/json')
        out = json.loads(response.data)
        self.assertGreater(len(out), 0)

    def test_no_search_seasons(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_by_seasons?s=0', content_type='application/json')
        out = json.loads(response.data)
        self.assertEqual(len(out), 0)

    def test_key_exists(self):
        test_app = app.test_client(self)
        key='058dc971daa1961eb8cdfbce2b18b816'
        response = test_app.get('/check_key?key={}'.format(key), content_type='application/json')
        out = json.loads(response.data)['status']
        self.assertEqual(out, 'Active')

    def test_show_keys(self):
        test_app = app.test_client(self)
        response = test_app.get('/show_keys', content_type='application/json')
        out = json.loads(response.data)
        self.assertGreater(len(out),0)

    def test_no_genre(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_by_genre?g=adfasf', content_type='application/json')
        out = json.loads(response.data)
        self.assertEquals(len(str(out)), 12)

    def test_search_genre(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_by_genre?g=action', content_type='application/json')
        out = json.loads(response.data)
        self.assertGreater(len(str(out)), 12)

    def test_wrong_episodes(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_ep?e=asds', content_type='application/json')
        out = json.loads(response.data)
        self.assertEqual(out, 'Error: Wrong Episode number')

    def test_no_episodes(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_ep?e=0', content_type='application/json')
        out = json.loads(response.data)
        self.assertEqual(len(str(out)), 12)

    def test_search_episodes(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_ep?e=1', content_type='application/json')
        out = json.loads(response.data)
        self.assertGreater(len(str(out)), 12)

    def test_show_all(self):
        test_app = app.test_client(self)
        response = test_app.get('/show_all', content_type='application/json')
        out = json.loads(response.data)
        self.assertGreater(len(out),0)

    def test_search_actors(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_by_actors?ac=amell', content_type='application/json')
        out = json.loads(response.data)
        self.assertGreater(len(out),0)

    def test_no_actors(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_by_actors?ac=adsfsdaf', content_type='application/json')
        out = json.loads(response.data)
        self.assertEqual(out,[])

    def test_space_actors(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_by_actors?ac=     ', content_type='application/json')
        out = json.loads(response.data)
        self.assertEqual(out,'Error: Please specify a name')

    def test_empty_actors(self):
        test_app = app.test_client(self)
        response = test_app.get('/search_by_actors?ac=', content_type='application/json')
        out = json.loads(response.data)
        self.assertEqual(out, 'Error: Please specify a name')

